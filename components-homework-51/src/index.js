import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Carts from './App';
import registerServiceWorker from './registerServiceWorker';
//
ReactDOM.render(<Carts />, document.getElementById('root'));
// ReactDOM.render(<Item1 />, document.getElementById('Carts'));
// ReactDOM.render(<Item2 />, document.getElementById('Carts'));
registerServiceWorker();
