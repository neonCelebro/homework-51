import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class Carts extends Component {
  constructor(props) {
    super(props);
    this.FILM_NAME = {
      film1: 'маска',
      film2: 'Бегущий по лезвию',
      film3: 'Хороший год'
    };
    this.FILM_YEAR = {
      film1: '1994г',
      film2: '1982г',
      film3: '2006г'
    };
    this.FILM_IMG = {
      film1: 'http://oc.kg/media/thumbnails/6/0/600bd75b10726e3e08ce4221b33859a2_200x300.jpg',
      film2: 'http://oc.kg/media/thumbnails/7/b/7b18945acec7cd1e8676ed1febd39177_200x284.jpg',
      film3: 'http://oc.kg/media/thumbnails/8/0/803f5c352b42200e3fb3c7e5b357a835_200x300.jpg'
    };
  }
  render() {
    return (
        <div className="Carts">
          <div className="Item">
            <img src={this.FILM_IMG.film1} className="App-logo" alt="logo" />
            <p className="title-name">{this.FILM_NAME.film1}</p>
            <span className="Year">{'год выпуска: '+ this.FILM_YEAR.film1}</span>
          </div>
          <div className="Item">
            <img src={this.FILM_IMG.film2} className="App-logo" alt="logo" />
            <p className="title-name">{this.FILM_NAME.film2}</p>
            <span className="Year">{'год выпуска: '+ this.FILM_YEAR.film2}</span>
          </div>
          <div className="Item">
            <img src={this.FILM_IMG.film3} className="App-logo" alt="logo" />
            <p className="title-name">{this.FILM_NAME.film3}</p>
            <span className="Year">{'год выпуска: '+ this.FILM_YEAR.film3}</span>
          </div>
        </div>
    );
  }
}

// class Item1 extends Carts {
//   constructor(props){
//     super(props);
//   }
//   render() {
//     return (
//         <div className="Item">
//             <img src={this.FILM_IMG.film1} className="App-logo" alt="logo" />
//             <p className="title-name">{this.FILM_NAME.film1}</p>
//           <span className="Year">{this.FILM_YEAR.film1}</span>
//         </div>
//     );
//   }
// };
//
// class Item2 extends Carts {
//   constructor(props) {
//     super(props);
//   }
//   render() {
//     return (
//       <div className="Item">
//           <img src={this.FILM_IMG.film1} className="App-logo" alt="logo" />
//           <p className="title-name">{this.FILM_NAME.film2}</p>
//         <span className="Year">{this.FILM_YEAR.film2}</span>
//       </div>
//     );
//   }
// }

//};

export default Carts;
