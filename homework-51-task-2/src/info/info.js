import React, { Component } from 'react';

class Info extends Component {
  constructor(props) {
  super(props);
  }
  render(){
    return(
      <div>
      <div className="info">
        <div className="info__textBox">
          <h3 className="info_MainText">WHY MODUS VERSUS?</h3>
          <p className="info_bottomText">Capacitance cascading integer reflective interface data development high bus cache dithering transponder.</p>
        </div>
        <div className="container container--flex--around">
          <div className="info_item info__item--1">
            <h5 className="info__mainText--row">Why Choose Us?</h5>
            <p className="info__text__item--1">Quisque at massa ipsum </p>
            <p className="info__text__item--1">Maecenas a lorem augue, egestas </p>
            <p className="info__text__item--1">Cras vitae felis at lacus eleifend</p>
            <p className="info__text__item--1">Etiam auctor diam pellentesque </p>
            <p className="info__text__item--1">Nulla ac massa at dolor </p>
            <p className="info__text__item--1">Condimentum eleifend vitae vitae  </p>
          </div>
          <div className="info_item info__item--2">
            <h5 className="info__mainText--row">About the project</h5>
            <p className="info__text__item--2">Curabitur quis nisl in leo euismod venenatis eu in diam. Etiam auctor diam pellentesque lectus <a href="#" >vehicula mattis.</a> Nulla ac massa at dolor condimentum eleifend vitae vitae urna. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui omnis vitae enim rerum similique consequuntur corporis molestias, error neque consequatur sed odio illum non expedita distinctio quod consectetur illo optio.</p>
          </div>
          <div className="info_item info__item--3">
          <h3 className="info__mainText--row">What Client’s Say?</h3>
            <p className="info__text__item--3">Curabitur quis nisl in leo euismod venenatis eu in diam. Etiam auctor diam pellentesque lectus vehicula mattis. Nulla ac massa at dolor condimentum</p>
            <p className="info__item--3--name info__mainText--row">Jhon Doe</p>
          </div>
        </div>
      </div>
    </div>
    )
  }
}

export default Info;
