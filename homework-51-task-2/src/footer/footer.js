import React, { Component } from 'react';

class Footer extends Component {
  constructor(props) {
  super(props);
  }
  render(){
    return(
      <footer class="footer">
    		<div class="container container--flex--around">
    			<div class="footer__item">
    				<h5 class="footer__item__mainText">MODUS</h5>
    				<span class="footer__item__MainText--1">versus</span>
    				<p class="footer__item__bottomText">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec .</p>
    			</div>
    			<div class="footer__item">
    				<h5 class="footer__item--2__MainText">Contacts</h5>
    				<p class="footer__item--2__bottomText">Phone :<span class="footer__item--2__bottomText--2">182 2569 5896</span></p>
    				<p class="footer__item--2__bottomText">e-mail:<span class="footer__item--2__bottomText--2">info@modu.versus</span></p>
    			</div>
    			<div class="footer__item">
    				<span class="footer__item--3__MainText">from the</span>
    				<h5 class="footer__item--3__MainText--3">BLOG</h5>
    				<a class="footer_item--3__link" href="#">
    					<img class="footer__item--3__img" alt="" src="../img/footer_row_img.jpg" alt="" width="69" height="70"/>
    					<p class="footer__item--3__text">Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
    					<p class="footer__item--3__date">26 May, 2013</p>
    				</a>
    			</div>
    		</div>
    		<div class="footer__low">
    			<div class="container">
    			<p class="footer__low__text">2013  ModusVersus</p>
    			</div>
    		</div>
    	</footer>
    )
  }
}
export default Footer;
