import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Header from './header/header';
import Promo from './promo/promo';
import Cervice from './service/service';
import Carts from './carts/carts';
import Info from './info/info';
import Footer from './footer/footer';


class App extends Component {
  render() {
    return (
      <div>
        <Header/>
        <Promo/>
        <Cervice/>
        <Carts/>
        <Info/>
        <Footer/>
      </div>
    );
  }
}

export default App;
