import React, { Component } from 'react';

class Promo extends Component {
  constructor(props) {
  super(props);
  }
  render(){
    return(
      <div>
      <img className="promo__bg" src="../img/promo_BG.jpg" alt="" />
    <div className="promo container--flex--center">
        <div className="promo__boxText">
          <h1 className="promo__MainText">VESTIBULUM</h1>
          <p className="promo__bottomText">Maecenas tincidunt, augue et rutrum condimentum, libero lectus mattis orci, ut commodo.</p>
        </div>
    </div>
    </div>
  )
}
}

export default Promo;
