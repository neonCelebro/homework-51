import React, { Component } from 'react';

class Header extends Component {
  constructor(props) {
  super(props);
  }
  render(){
    return(
      <header>
    		<div className="header">
    			<div className="container">
    				<a href="#" className="header__logo">MODUS<span className="header__logo--sm">versus</span></a>
    				<ul className="header__nav">
    					<li className="header__nav-element"><a href="#" className="header__nav-link">Home</a></li>
    					<li className="header__nav-element"><a href="#" className="header__nav-link">about</a></li>
    					<li className="header__nav-element"><a href="#" className="header__nav-link">services</a></li>
    					<li className="header__nav-element"><a href="#" className="header__nav-link"   >blog</a></li>
    				</ul>
    			</div>
    		</div>
    	</header>
    )
  }
}

export default Header;
