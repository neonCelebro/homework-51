import React, { Component } from 'react';

class Cervice extends Component {
  constructor(props) {
  super(props);
  }
  render(){
    return(
      <div className="service container">
        <h3 className="service__mainText">Some of our top services</h3>
        <p className="service__bottomText">Ut eleifend libero sed neque rhoncus consequat. Maecenas tincidunt, augue et rutrum condimentum, libero lectus mattis orci, ut commodo. </p>
        <a className="service__btn" href="#">VIEW MORE</a>
      </div>
    )
  }
}

export default Cervice;
