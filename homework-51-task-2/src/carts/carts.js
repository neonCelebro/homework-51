import React, { Component } from 'react';

class Carts extends Component {
  constructor(props) {
  super(props);
  }
  render(){
    return(
      <div className="carts">
      <div className="container container--flex--around">
        <div className="carts__item">
          <a className="carts__icon" href="#"><i className="fa fa-thumbs-o-up" aria-hidden="true"></i></a>
          <p className="carts__mainText carts__mainText--item_1">SUSPENDISSE</p>
          <p className="carts_bottomText"> Quisque id tellus quis risus vehicula vehicula ut turpis. In eros nulla, placerat vitae at, vehicula ut nunc.</p>
          <a className="carts__btn" href="#">read more</a>
        </div>
        <div className="carts__item">
          <a className="carts__icon" href="#"><i className="fa fa-key" aria-hidden="true"></i></a>
          <p className="carts__mainText carts__mainText--item_2">MAECENAS</p>
          <p className="carts_bottomText">Ut eleifend libero sed neque rhoncus consequat. Maecenas tincidunt, augue et rutrum condimentum, libero lectus mattis orci, ut commodo.</p>
          <a className="carts__btn" href="#">read more</a>
        </div>
        <div className="carts__item">
          <a className="carts__icon" href="#"><i className="fa fa-flag" aria-hidden="true"></i></a>
          <p className="carts__mainText carts__mainText--item_3">ALIQUAM</p>
          <p className="carts_bottomText">Vivamus eget ante bibendum arcu vehicula ultricies. Integer venenatis mattis nisl, vitae pulvinar dui tempor non.</p>
          <a className="carts__btn" href="#">read more</a>
        </div>
        <div className="carts__item">
          <a className="carts__icon" href="#"><i className="fa fa-flask" aria-hidden="true"></i></a>
          <p className="carts__mainText carts__mainText--item_4">HABITASSE</p>
          <p className="carts_bottomText">Astehicula ultricies. Integer venenatis mattis nisl, vitae pulvinar dui tempor non.</p>
          <a className="carts__btn" href="#">read more</a>
        </div>
      </div>
      </div>
    )
  }
}
export default Carts;
